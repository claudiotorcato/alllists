"""criar campo ship_id em pilots

Revision ID: 1c6618469a78
Revises: 29a250992b76
Create Date: 2016-11-14 23:01:58.851688

"""

# revision identifiers, used by Alembic.
revision = '1c6618469a78'
down_revision = '29a250992b76'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('pilots', sa.Column('ship_id', sa.Integer))


def downgrade():
    pass
