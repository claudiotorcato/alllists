"""criar campo expansion_id em pilot_expansions

Revision ID: 4c51060de62a
Revises: 2092a74c33a9
Create Date: 2016-11-09 22:37:04.152428

"""

# revision identifiers, used by Alembic.
revision = '4c51060de62a'
down_revision = '2092a74c33a9'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('pilot_expansions', sa.Column('expansion_id', sa.Integer))


def downgrade():
    pass
