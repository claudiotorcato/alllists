"""criar campo expansion_id em upgrade_expansions

Revision ID: 29a250992b76
Revises: 4c51060de62a
Create Date: 2016-11-09 22:59:31.323628

"""

# revision identifiers, used by Alembic.
revision = '29a250992b76'
down_revision = '4c51060de62a'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('upgrade_expansions', sa.Column('expansion_id', sa.Integer))


def downgrade():
    pass
