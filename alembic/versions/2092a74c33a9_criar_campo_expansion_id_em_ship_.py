"""criar campo expansion_id em ship_expansions

Revision ID: 2092a74c33a9
Revises: None
Create Date: 2016-10-28 00:05:45.279800

"""

# revision identifiers, used by Alembic.
revision = '2092a74c33a9'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('ship_expansions', sa.Column('expansion_id', sa.Integer))


def downgrade():
    op.drop_column('ship_expansions', 'expansion_id')
