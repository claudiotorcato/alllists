﻿import string
import requests        
import time
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import and_
from concoction import app

class Torneio:
    def __init__(self, elementos, id):
        self.id = id
        self.elementos = elementos
        self.nome = elementos[0].torneio
        self.quando = elementos[0].data_torneio
        self.tipo_torneio = elementos[0].tipo_torneio
        self.jogadores = self.extrair_jogadores()
        for jogador in self.jogadores:
            self.extrair_lista(jogador)

    def extrair_jogadores(self):
        nomes = []
        jogadores = []
        for e in self.elementos:
            if e.jogador and e.jogador not in nomes:
                nomes.append(e.jogador)
        for nome in nomes:
            jogadores.append(Jogador(nome))
        return jogadores

    def extrair_lista(self, jogador):
        lista = []
        faccao = ''
        posicao = None
        posicao_suico = None
        for e in self.elementos:
            if e.jogador == jogador.nome:
                lista.append(Nave(e))
                faccao = e.faccao
                if e.posicao_eliminatoria:
                    posicao = e.posicao_eliminatoria
                if e.posicao_suico:
                    posicao_suico = e.posicao_suico
        jogador.adicionarLista(lista)
        jogador.adicionarFaccao(faccao)
        jogador.adicionarPosicao(posicao)
        jogador.adicionarPosicaoSuico(posicao_suico)

class Jogador:
    def __init__(self, nome, faccao='', posicao = None, posicao_suico = None, lista = None):
        self.nome = nome
        self.lista = lista
        self.faccao = faccao
        self.posicao = posicao
        self.posicao_suico = posicao_suico
    def adicionarLista(self, lista):
        self.lista = lista
    def adicionarFaccao(self, faccao):
        self.faccao = faccao
    def adicionarPosicao(self, posicao):
        self.posicao = posicao
    def adicionarPosicaoSuico(self, posicao_suico):
        self.posicao_suico = posicao_suico

class Upgrade:
    def __init__(self, nome, tipo):
        self.nome = nome
        self.tipo = tipo
    def __repr__(self):
        return '{}({})'.format(self.nome, self.tipo)

class Piloto:
    def __init__(self, nome, faccao = None):
        self.nome = nome
        self.faccao = faccao

    def __repr__(self):
        return self.nome

class Nave:
    def __init__(self, elemento):
        self.nome = elemento.nave
        self.piloto = Piloto(elemento.piloto)
        self.upgrades = []
        if elemento.tripulacao_1:
            self.addUpgrade(elemento.tripulacao_1, 'crew')
        if elemento.tripulacao_2:
            self.addUpgrade(elemento.tripulacao_2, 'crew')
        if elemento.tripulacao_3:
            self.addUpgrade(elemento.tripulacao_3, 'crew')
        if elemento.titulo:
            self.addUpgrade(elemento.titulo, 'title')
        if elemento.talento_elite_1:
            self.addUpgrade(elemento.talento_elite_1, 'elite')
        if elemento.talento_elite_2:
            self.addUpgrade(elemento.talento_elite_2, 'elite')
        if elemento.astromech:
            self.addUpgrade(elemento.astromech, 'astromech')
        if elemento.sistema:
            self.addUpgrade(elemento.sistema, 'system')
        if elemento.modificacao_1:
            self.addUpgrade(elemento.modificacao_1, 'modification')
        if elemento.modificacao_2:
            self.addUpgrade(elemento.modificacao_2, 'modification')
        if elemento.canhao:
            self.addUpgrade(elemento.canhao,'cannon')
        if elemento.missil_1:
            self.addUpgrade(elemento.missil_1,'missile')
        if elemento.missil_2:
            self.addUpgrade(elemento.missil_2, 'missile')
        if elemento.torpedo_1:
            self.addUpgrade(elemento.torpedo_1, 'torpedo')
        if elemento.torpedo_2:
            self.addUpgrade(elemento.torpedo_2, 'torpedo')
        if elemento.bomba:
            self.addUpgrade(elemento.bomba, 'bomb')
        if elemento.turret:
            self.addUpgrade(elemento.turret, 'turret')
        if elemento.tech:
            self.addUpgrade(elemento.tech, 'tech')
        if elemento.astromech_selvagem:
            self.addUpgrade(elemento.astromech_selvagem, 'salvaged_astromech')
        if elemento.ilicito:
            self.addUpgrade(elemento.ilicito, 'illicit')

    def addUpgrade(self, nome, tipo):
        self.upgrades.append(Upgrade(nome, tipo))

    def __repr__(self):
        return '{} {} {}'.format(self.nome, self.piloto, self.upgrades)

class Lista:
    def __init__(self, lista):
        self.jogador = None
        self.naves = []
        self.pontos = 0

class Elemento:
    def __init__(self, lista):
        self.torneio = lista[0]
        self.tipo_torneio = lista[1]
        self.data_torneio = lista[2]
        self.jogador = lista[3]
        self.faccao = lista[4]
        self.pontos = lista[5]
        self.posicao_suico = lista[6]
        self.posicao_eliminatoria = lista[7]
        self.id_lista = lista[8]
        self.nave = lista[9]
        self.piloto = lista[10]
        self.talento_elite_1 = lista[11]
        self.talento_elite_2 = lista[12]
        self.titulo = lista[13]
        self.tripulacao_1 = lista[14]
        self.tripulacao_2 = lista[15]
        self.tripulacao_3 = lista[16]
        self.astromech = lista[17]
        self.sistema = lista[18]
        self.modificacao_1 = lista[19]
        self.modificacao_2 = lista[20]
        self.canhao = lista[21]
        self.missil_1 = lista[22]
        self.missil_2 = lista[23]
        self.torpedo_1 = lista[24]
        self.torpedo_2 = lista[25]
        self.bomba = lista[26]
        self.turret = lista[27]
        self.tech = lista[28]
        self.astromech_selvagem = lista[29]
        self.ilicito = lista[30]
    def __repr__(self):
        return '{} {} {}'.format(self.id_lista, self.piloto, self.nave)

def montar_lista(elementos):
    listas = {}
    for elemento in elementos:
        if elemento.id_lista:
            if listas.has_key(elemento.id_lista):
                listas[elemento.id_lista].append(elemento)
            else:
                listas[elemento.id_lista] = [elemento]
    return listas

def montar_naves(elementos):
    pass

def montar_jogadores(elementos):
    pass

class Extrator:

    def __init__(self, ids_torneio):
        self.ids_torneio = ids_torneio
        self.torneios = []

    def extrair(self):
        for id in self.ids_torneio:
            g = requests.get(
                'http://lists.starwarsclubhouse.com/export_tourney_lists?tourney_id=%d' % id,
                proxies=app.config['PROXIES'])
            print 'Status', g.status_code

            if g.status_code == 200:
                texto = g.text
                lista = string.split(texto, '\n')
                campos = string.split(lista[0],',')
                objetos = [i for i in lista[-(len(lista)-1):] if i]
                elementos = []
                for i in objetos:
                    valores = string.split(i, ',')
                    #print valores
                    elemento = Elemento(valores)
                    elementos.append(elemento)
                #listas = montar_lista(elementos)
                if elementos:
                    torneio = Torneio(elementos, id)
                    self.torneios.append(torneio)
                #time.sleep(5)

def save(extrator, session):
    for torneio in extrator.torneios:
        novo_torneio = Tourney.query.filter(Tourney.id_url == torneio.id).first()
        if novo_torneio:
            print 'Torneio já existe'
            continue
        novo_torneio = Tourney(name=torneio.nome, id_url=torneio.id, when=torneio.quando, format=torneio.tipo_torneio)
        session.add(novo_torneio)
        session.commit()
        try:
            print 'Torneio [', novo_torneio.id_url, '] ', novo_torneio.name, 'foi cadastrado.'
        except:
            print 'Torneio [', novo_torneio.id_url, '] ', 'foi cadastrado.'
        for jogador in torneio.jogadores:
            nova_lista = List.query.filter(and_(List.name == jogador.nome, List.tourney_id == novo_torneio.id)).first()
            if nova_lista:
                print 'Lista foi cadastrada anteriormente'
                continue
            nova_lista = List(name=jogador.nome, faction=jogador.faccao, position=jogador.posicao, 
                position_swiss=jogador.posicao_suico, tourney=novo_torneio)
            session.add(nova_lista)
            session.commit()
            for nave in jogador.lista:
                nova_nave = Ship.query.filter(Ship.name == nave.nome).first()
                if not nova_nave:
                    nova_nave = Ship(name=nave.nome)
                    session.add(nova_nave)
                    session.commit()
                novo_piloto = Pilot.query.filter(and_(Pilot.name == nave.piloto.nome, Pilot.faction == jogador.faccao)).first()
                if not novo_piloto:
                    novo_piloto = Pilot(name=nave.piloto.nome, faction=jogador.faccao,
                        ship_id=nova_nave.id)
                    session.add(novo_piloto)
                    session.commit()
                else:
                    if not novo_piloto.ship_id:
                        novo_piloto.ship_id = nova_nave.id
                        session.commit()
                nova_list_ship = ListShip(list_id = nova_lista.id, ship_id = nova_nave.id, pilot_id = novo_piloto.id)
                session.add(nova_list_ship)
                session.commit()
                for upgrade in nave.upgrades:
                    novo_card = Card.query.filter(and_(Card.name == upgrade.nome,Card.typo == upgrade.tipo)).first()
                    if not novo_card:
                        novo_card = Card(name=upgrade.nome, typo=upgrade.tipo)
                        session.add(novo_card)
                        session.commit()
                    upgrade_ship = UpgradeShip(list_ship_id = nova_list_ship.id, card_id = novo_card.id)
                    session.add(upgrade_ship)
                    session.commit()

from concoction.models import Base, Tourney, List, Card, Ship, Pilot, ListShip, UpgradeShip
engine = create_engine(app.config['DATABASE_URI'], convert_unicode=True)
Base.metadata.bind = engine
DBSession = scoped_session(sessionmaker(autocommit=False,
                                     autoflush=False,
                                     bind=engine))    
Base.query = DBSession.query_property()
session = DBSession()

for id in range(1,2302):
    extrator = Extrator([id])
    extrator.extrair()
    save(extrator, session)
session.close()