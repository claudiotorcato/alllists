# -*- coding: utf-8 -*-
from database import db_session, init_db
from models import Ship, Card, Pilot, Tourney, List, ListShip, UpgradeShip, Expansion
from models import ShipExpansion, PilotExpansion, UpgradeExpansion
from flask import Flask, render_template, redirect, url_for, flash, request
from flask_restful import Resource, Api

from concoction import app


api = Api(app)


class ShipResource(Resource):
	def get(self, nave_id):
		nave = Ship.query.filter(Ship.id == nave_id).first()
		return {'id': dict(name=nave.name)}

class ShipListResource(Resource):
	def get(self, name):
		naves = Ship.query.filter(Ship.name.like('%' + name + '%'))
		lista = {}
		for nave in naves:
			lista[nave.id] = dict(name=nave.name)
		return lista

api.add_resource(ShipResource, '/<int:nave_id>')
api.add_resource(ShipListResource, '/ships/<name>')