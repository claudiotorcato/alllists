# -*- coding: utf-8 -*-

from flask import Flask
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from database import db_session, init_db
from models import PilotExpansion, ShipExpansion, UpgradeExpansion, Card, Pilot
from models import Tourney, List, ListShip, Ship

app = Flask(__name__)
app.config.from_object('concoction.configmodule.CasaConfig')
try:
	app.config.from_envvar('XWINGCONCOCTION_SETTINGS')
except RuntimeError:
	pass

@app.teardown_appcontext
def shutdown_session(exception=None):
	db_session.remove()


admin = Admin(app, name='concoction', template_mode = 'bootstrap3')

class ConcoctionPilotModelView(ModelView):
	column_searchable_list = ['name',]
	form_choices = {
    'faction': [
        ('Rebel Alliance','Rebel Alliance'),
        ('Galactic Empire','Galactic Empire'),
        ('Scum and Villainy','Scum and Villainy')
    ]
}

class ConcoctionUpgradeModelView(ModelView):
	column_searchable_list = ['name',]

class ConcoctionShipModelView(ModelView):
	column_searchable_list = ['name',]

class ConcoctionUpgradeExpansionModelView(ModelView):
	column_searchable_list = ['upgrade.name','expansion.name']	
	column_filters = ['expansion.name','upgrade.typo']

class ConcoctionPilotExpansionModelView(ModelView):
	column_filters = ['expansion.name','pilot.name']

admin.add_view(ModelView(Tourney, db_session))
admin.add_view(ModelView(List, db_session))
admin.add_view(ModelView(ListShip, db_session))
admin.add_view(ConcoctionPilotExpansionModelView(PilotExpansion, db_session))
admin.add_view(ModelView(ShipExpansion, db_session))
admin.add_view(ConcoctionUpgradeExpansionModelView(UpgradeExpansion, db_session))
admin.add_view(ConcoctionUpgradeModelView(Card, db_session))
admin.add_view(ConcoctionPilotModelView(Pilot, db_session))
admin.add_view(ConcoctionShipModelView(Ship, db_session))

import concoction.views