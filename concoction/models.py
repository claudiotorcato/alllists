from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship
from database import Base

class Ship(Base):
    __tablename__ = 'ships'

    id = Column(Integer, primary_key=True)
    name = Column(String)

    def __repr__(self):
        return '{0}'.format(self.name)

class Card(Base):
    __tablename__ = 'cards'
    
    id = Column(Integer, primary_key=True)
    name = Column(String)
    faction = Column(String)
    typo = Column(String)

    def __repr__(self):
        return '{0} [{1}]'.format(self.name, self.typo)
    
class Pilot(Base):
    __tablename__ = 'pilots'
    
    id = Column(Integer, primary_key=True)
    name = Column(String)
    faction = Column(String)
    ship_id = Column(Integer, ForeignKey('ships.id'))
    ship = relationship(Ship)

    def __repr__(self):
        if not self.ship:
            return '{0} [{1}]'.format(self.name, self.faction)
        else:
            return '{0} [{1}] {2}'.format(self.name, self.faction, self.ship.name)

class Tourney(Base):
    __tablename__ = 'tourneys'

    id = Column(Integer, primary_key=True)
    id_url = Column(Integer)
    name = Column(String)
    when = Column(String)
    format = Column(String)

    def __repr__(self):
        return '{0}'.format(self.name)

class List(Base):
    __tablename__ = 'lists'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    faction = Column(String)
    position = Column(String)
    position_swiss = Column(String)
    tourney_id = Column(Integer, ForeignKey('tourneys.id'))
    tourney = relationship(Tourney)

    def __repr__(self):
        return '{0} [{1}]'.format(self.name, self.tourney.name)
    
class ListShip(Base):
    __tablename__ = 'lists_ship'
    
    id = Column(Integer, primary_key=True)
    list_id = Column(Integer, ForeignKey('lists.id'))
    list = relationship(List)
    ship_id = Column(Integer, ForeignKey('ships.id'))
    ship = relationship(Ship)
    pilot_id = Column(Integer, ForeignKey('pilots.id'))
    pilot = relationship(Pilot)    
    
class UpgradeShip(Base):
    __tablename__ = 'upgrades_ship'
    
    id = Column(Integer, primary_key=True)
    list_ship_id = Column(Integer, ForeignKey('lists_ship.id'))
    list_ship = relationship(ListShip)
    card_id = Column(Integer, ForeignKey('cards.id'))
    card = relationship(Card)
    
class Expansion(Base):
    __tablename__ = 'expansions'
    
    id = Column(Integer, primary_key=True)
    name = Column(String)
    wave = Column(String)

    def __repr__(self):
        if self.wave:
            return '{0} [{1}]'.format(self.name, self.wave)
        else:
            return '{0}'.format(self.name)
    
class ShipExpansion(Base):
    __tablename__ = 'ship_expansions'
    
    id = Column(Integer, primary_key=True)
    ship_id = Column(Integer, ForeignKey('ships.id'))
    ship = relationship(Ship)
    expansion_id = Column(Integer, ForeignKey('expansions.id'))
    expansion = relationship(Expansion)
    quantity = Column(Integer)
    
class PilotExpansion(Base):
    __tablename__ = 'pilot_expansions'
    
    id = Column(Integer, primary_key=True)
    pilot_id = Column(Integer, ForeignKey('pilots.id'))
    pilot = relationship(Pilot)
    expansion_id = Column(Integer, ForeignKey('expansions.id'))
    expansion = relationship(Expansion)    
    quantity = Column(Integer)
    
class UpgradeExpansion(Base):
    __tablename__ = 'upgrade_expansions'
    
    id = Column(Integer, primary_key=True)
    upgrade_id = Column(Integer, ForeignKey('cards.id'))
    upgrade = relationship(Card)
    expansion_id = Column(Integer, ForeignKey('expansions.id'))
    expansion = relationship(Expansion)    
    quantity = Column(Integer)

