import proxies

class Config(object):
	DEBUG = True
	DATABASE_URI = 'sqlite:///database.db'
	SECRET_KEY=b'A*\x86\xd1%\x1c<\x84\xc7\xdb/\xe2hJ\x01\x8d\xd3Z\xd2\x94\x11%a\xa2'
	PROXIES = {}

class TrabalhoConfig(Config):
	DATABASE_URI = 'sqlite:///database_local.db'
	PROXIES = proxies.PROXIES

class CasaConfig(Config):
	DATABASE_URI = 'sqlite:///database_local.db'