from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import and_
from concoction import app
from concoction.models import Base, Tourney, List, Card, Ship, Pilot, ListShip, UpgradeShip
engine = create_engine(app.config['DATABASE_URI'], convert_unicode=True)
Base.metadata.bind = engine
DBSession = scoped_session(sessionmaker(autocommit=False,
                                     autoflush=False,
                                     bind=engine))    
Base.query = DBSession.query_property()
session = DBSession()

#  | unidades | expansao |
#  | 

result = session.execute(
	"""
	select se.*, ls.* from ship_expansions se join lists_ship ls ...

	select * from expansions where id in (
	select expansion_id from ship_expansions where ship_id in (
	select ship_id from lists_ship where list_id =10))
	""")

for i in result:
	print i

session.close()